#!/bin/bash
BIN="/usr/local/bin/k3s"

# Default K3S_VERSION to latest
if [ -n "${K3S_VERSION}" ]; then
    K3S_VERSION="${K3S_VERSION/+/%2B}"
    URL="https://github.com/rancher/k3s/releases/download/${K3S_VERSION}/k3s"
else
    >&2 echo "Warning: No K3S_VERSION specified. Using latest"
    K3S_VERSION="latest"
    URL="https://github.com/rancher/k3s/releases/latest/download/k3s"
fi

echo "Downloading ${URL} to ${BIN}"
curl -Lso "${BIN}" "${URL}"
[ ! -f "${BIN}" ] && >&2 echo "Downloading ${URL} failed for some reason. ${BIN} not found!" && exit 1

chmod +x "${BIN}"
"${BIN}" --version
