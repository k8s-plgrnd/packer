#!/bin/sh
export DEBIAN_FRONTEND=noninteractive

# Install common software
apt-get update
apt-get upgrade -y
apt-get install \
  apt-transport-https \
  ca-certificates \
  curl \
  software-properties-common \
  ufw \
  -yq

# IP Forwarding
sysctl -w net.ipv4.ip_forward=1
sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
sysctl -p /etc/sysctl.conf

# Disable swap
swapoff -a
sed -i '/^[^#].*\sswap\s/s/^/#/' /etc/fstab

# Block all but ssh
ufw allow to any port 22 proto tcp
sed -i 's/^DEFAULT_FORWARD_POLICY=.*/DEFAULT_FORWARD_POLICY="ACCEPT"/' /etc/default/ufw
yes | ufw enable
systemctl enable ufw
