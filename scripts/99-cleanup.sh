#!/bin/bash

# Borrowed a bit from
# https://git.osuosl.org/osuosl/packer-templates/-/blob/21f889df3fd33aac6a811f3de13ffbe27257b746/scripts/ubuntu/cleanup.sh

# Delete all Linux headers
dpkg --list \
  | awk '{ print $2 }' \
  | grep 'linux-headers' \
  | xargs apt-get -y purge;

# Remove specific Linux kernels, such as linux-image-3.11.0-15 but
# keeps the current kernel and does not touch the virtual packages,
# e.g. 'linux-image-amd64', etc.
dpkg --list \
    | awk '{ print $2 }' \
    | grep 'linux-image-.*-generic' \
    | grep -v `uname -r` \
    | xargs apt-get -y purge;

# Remove linux modules
dpkg --list \
    | awk '{ print $2 }' \
    | grep 'linux-modules-.*-generic' \
    | grep -v `uname -r` \
    | xargs apt-get -y purge;

# Delete Linux source
dpkg --list \
    | awk '{ print $2 }' \
    | grep linux-source \
    | xargs apt-get -y purge;

rm -rf /var/lib/command-not-found/*
apt-get -y purge unattended-upgrades friendly-recovery popularity-contest bash-completion command-not-found

# Delete the massive firmware packages
rm -rf /lib/firmware/*
rm -rf /usr/share/doc/linux-firmware/*

apt-get autoremove -y
apt-get clean
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

find /var/cache -type f -exec rm -rf {} \;
find /var/log/ -type f -exec truncate -s 0 {} \;

truncate -s 0 /etc/machine-id

cat /dev/null > /root/.bash_history && history -c && history -w
